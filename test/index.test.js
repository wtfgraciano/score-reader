const { promisify } = require('util')
const fs = require('fs')

const readFileAsync = promisify(fs.readFile)
const filePath = './test/example.xml'
const mockHTMLPlayer = jest.fn()
const mockOSMDConstructor = jest.fn()
const mockOSMDRender = jest.fn()
const mockReturnHTMLPlayer = {}

jest.mock('../src/createHTMLPlayer', () => ({
  createHTMLPlayer: jest.fn().mockImplementation(async (data) => {
    mockHTMLPlayer(data)
    return mockReturnHTMLPlayer
  })
}))

jest.mock('opensheetmusicdisplay', () => ({
  OpenSheetMusicDisplay: mockOSMDConstructor.mockImplementation(() => ({
    render: mockOSMDRender
  }))
}))

const { tanana } = require('../index')

//  should create a player given music xml data
describe('tanana', () => {
  let data
  beforeEach(async () => {
    data = await readFileAsync(filePath)
  })

  it('throws an error if the file doesn\'t exists', () => {
    expect(tanana()).rejects.toBeInstanceOf(Error)
  })

  it('creates an osmd', async () => {
    await tanana(data)
    expect(mockOSMDConstructor).toHaveBeenCalled()
  })

  it('gives the data to the osmd', async () => {
    await tanana(data)
    expect(mockOSMDConstructor).toHaveBeenCalledWith(data)
  })

  it('renders the osmd', async () => {
    await tanana(data)
    expect(mockOSMDRender).toHaveBeenCalled()
  })

  it('creates the HTML player', async () => {
    await tanana(data)
    expect(mockHTMLPlayer).toHaveBeenCalled()
  })

  it('returns the HTML player', async () => {
    const htmlPlayer = await tanana(data)
    expect(htmlPlayer).toEqual(mockReturnHTMLPlayer)
  })
})
