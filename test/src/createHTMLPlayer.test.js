const { promisify } = require('util')
const fs = require('fs')
const { OpenSheetMusicDisplay } = require('opensheetmusicdisplay')

const readFileAsync = promisify(fs.readFile)
const filePath = './test/example.xml'

jest.mock('opensheetmusicdisplay', () => ({
  OpenSheetMusicDisplay: jest.fn()
}))

const { createHTMLPlayer } = require('../../src/createHTMLPlayer')

describe('createHTMLPlayer', () => {
  let osmd
  beforeEach(async () => {
    const data = await readFileAsync(filePath)
    osmd = new OpenSheetMusicDisplay(data)
  })

  it('returns an html element', async () => {
    const player = await createHTMLPlayer(osmd)
    expect(player).toBeInstanceOf(HTMLElement)
  })

  it('has a svg score element', async () => {
    const player = await createHTMLPlayer(osmd)
    const svg = player.querySelector('svg')
    expect(svg).toBeInstanceOf(HTMLElement)
  })

  it('has a play/pause button', async () => {
    const player = await createHTMLPlayer(osmd)
    const button = player.querySelector('button')
    expect(button).toBeInstanceOf(HTMLElement)
  })
})
