const { OpenSheetMusicDisplay } = require('opensheetmusicdisplay')
const { createHTMLPlayer } = require('./src/createHTMLPlayer')

const tanana = async data => {
  if (!data) {
    throw new Error('invalid data')
  }
  const osmd = new OpenSheetMusicDisplay(data)
  osmd.render()
  return createHTMLPlayer(osmd)
}

module.exports = {
  tanana
}
